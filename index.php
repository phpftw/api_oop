<?php
require_once 'header.php'
?>

    <section class="main-container">
    <div class="main-wrapper">
    <h2>Main</h2>
<?php
if (!isset($_SESSION['id'])) {
    echo 'You have to login in order to see content';
} else {
    require_once 'includes/database.php';
    require_once 'includes/pair.php';
    require_once 'includes/api.php';
    require_once 'includes/dbconfig.php';

    $api = new ApiData();
    $db = new Database($servername, $username, $password, $dbname);
    $pairs = new Pair($db);

    $eurusd = $api->getData('EURUSD');
    $gbpjpy = $api->getData('GBPJPY');
    $audusd = $api->getData('AUDUSD');

    $pairs->add($eurusd);
    $pairs->add($gbpjpy);
    $pairs->add($audusd);

    $pairs->show('EURUSD');
}
?>
</section>
</div>
<?php
require_once 'footer.php';
?>
