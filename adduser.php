<?php
$first = $last = $email = $uid = $pwd = $confirmpwd = '';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    require_once 'includes/user.php';
    require_once 'includes/database.php';
    require_once 'includes/dbconfig.php';

    $first = $_POST['first'];
    $last = $_POST['last'];
    $email = $_POST['email'];
    $uid = $_POST['uid'];
    $pwd = $_POST['pwd'];
    $confirmpwd = $_POST['confirmpwd'];

    $db = new Database($servername, $username, $password, $dbname);
    $user = new User($db);

//Check for input errors
    $error = $user->validate($first, $last, $email, $uid, $pwd, $confirmpwd);

//If there are no errors add User data to database
    if ($error == null) {
        $user->add();
        header("Location: index.php");
    }
}
