<?php
require_once 'header.php';
require_once 'edit.php';
?>
<section class="main-container">
    <div class="main-wrapper">
    <h2>Edit  Profile</h2>
    <form class="signup-form" action="edituser.php" method="POST">
<?php if (isset($error['first'])): ?><span class="errormsg"><?=$error['first']?></span><?php endif;?>
    <input type="text" name="editfirst" placeholder="Edit Firstname" value="<?=$row['first']?>">
<?php if (isset($error['last'])): ?><span class="errormsg"><?=$error['last']?></span><?php endif;?>
    <input type="text" name="editlast" placeholder="Edit Lastname"value="<?=$row['last']?>">
<?php if (isset($error['email'])): ?><span class="errormsg"><?=$error['email']?></span><?php endif;?>
    <input type="text" name="editemail" placeholder="Edit E-mail"value="<?=$row['email']?>">
<?php if (isset($error['uid'])): ?><span class="errormsg"><?=$error['uid']?></span><?php endif;?>
    <input type="text" name="edituser" placeholder="Edit Username"value="<?=$row['user']?>">
<?php if (isset($error['pwd'])): ?><span class="errormsg"><?=$error['pwd']?></span><?php endif;?>
    <input type="password" name="editpwd" placeholder="New Password">
    <input type="password" name="editconfirmpwd" placeholder="Confirm New Password">
    <button type="submit" name="edit">Save</button>

</section>
</div>
<?php require_once 'footer.php';
?>
