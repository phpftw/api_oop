<?php
require_once 'header.php';
require_once 'includes/user.php';
require_once 'includes/database.php';
require_once 'includes/dbconfig.php';
$db = new Database($servername, $username, $password, $dbname);
$profileuser = new User($db);

$id = $_SESSION['id'];
$row = $profileuser->getById($id);
?>
  <section class="main-container">
    <div class="main-wrapper">
    <h2><?=$row['user']?>'s Profile</h2>
    <span class="user-profile">Firstname : <?=$row['first']?>  </span>
    <span class="user-profile">Lastname : <?=$row['last']?>  </span>
    <span class="user-profile">E-mail : <?=$row['email']?>  </span>
    <span class="user-profile">Username : <?=$row['user']?>  </span>
    <span class="user-profile">Join Date : <?=$row['date']?>  </span>
    <form class="user-form" action='edituser.php' method="POST">
    <button type="submit" name="submit">Edit Profile</button>
    </form>
    </section>
<?php
require_once 'footer.php';
?>
