<?php
require_once 'header.php'
?>

    <section class="main-container">
    <div class="main-wrapper">
    <h2>Login</h2>
<?php
require_once 'includes/user.php';
require_once 'includes/database.php';
require_once 'includes/dbconfig.php';

$uidemail = $_POST['uidemail'];
$pwd = $_POST['pwd'];

$db = new Database($servername, $username, $password, $dbname);
$user = new User($db);

$error = $user->login($uidemail, $pwd);

if (isset($error['uidemail'])) {echo '<span class="login-error">' . $error['uidemail'] . '</span></br>';}
if (isset($error['pwd'])) {echo '<span class="login-error">' . $error['pwd'] . '</span></br>';}

if ($error == null) {
    header("Location: welcome.php");
    die();
}

?>
</section>
<?php
require_once 'footer.php';
?>
