<?php
require_once 'includes/user.php';
require_once 'includes/database.php';
require_once 'includes/dbconfig.php';
$db = new Database($servername, $username, $password, $dbname);
$edituser = new User($db);

$id = $_SESSION['id'];
$row = $edituser->getById($id);

if (isset($_POST['edit'])) {
    $first = $_POST['editfirst'];
    $last = $_POST['editlast'];
    $email = $_POST['editemail'];
    $user = $_POST['edituser'];
    $pwd = $_POST['editpwd'];
    $confirmPwd = $_POST['editconfirmpwd'];

    //Check for input errors
    $error = $edituser->editvalidate($id, $first, $last, $email, $user, $pwd, $confirmPwd);

    //If there are no errors edit User data in the database
    if ($error == null) {
        $edituser->edit($id, $first, $last, $email, $user, $pwd);
    }
}
