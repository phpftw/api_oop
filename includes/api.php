<?php
class ApiData
{
    private $data;
    private function getApi($pair)
    {
        $url = 'https://forex.1forge.com/1.0.3/quotes?pairs=' . $pair . '&api_key=UPv8GZdb1sSDpJG1MSRcKsdN1UyXkfa7';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $this->data = curl_exec($curl);
        curl_close($curl);
        return;
    }
    public function getData($pair)
    {
        $this->getApi($pair);
        $decoded = json_decode($this->data, true);
        return $decoded;
    }
}
