<?php

class Pair
{
    private $db;
    private $symbol;
    private $price;
    private $timestamp;
    public function __construct(DB $db)
    {
        $this->db = $db;
    }
    public function add($data)
    {
        $this->symbol = $data[0]['symbol'];
        $this->price = $data[0]['price'];
        $this->timestamp = $data[0]['timestamp'];
        $statement = "INSERT INTO pairs (symbol,price,timestamp)
        VALUES ('$this->symbol','$this->price','$this->timestamp')";
        $this->db->query($statement);
    }

    public function getAll()
    {
        $statement = "SELECT * FROM pairs";
        $result = $this->db->query($statement);
        while ($row = $result->fetch()) {
            $data[] = $row;
        }
        return $data;
    }
    public function get($symbol)
    {
        $sql = "SELECT * FROM pairs WHERE symbol=?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([$symbol]);
        $row = $stmt->fetchAll();
        return $row;

    }
    public function show($symbol)
    {
        $row = $this->get($symbol);
        echo '<table class="pair-table">
            <tr bgcolor="white">
			<th>SYMBOL</th>
			<th>PRICE</th>
			<th>Date/Time</th>
			</tr>' . PHP_EOL;
        for ($i = 0; $i < count($row); $i++) {
            echo '<tr>' . PHP_EOL;
            echo '<td>', print_r($row[$i]['symbol'], 1), '</td>' . PHP_EOL;
            echo '<td>', print_r($row[$i]['price'], 1), '</td>' . PHP_EOL;
            echo '<td>', print_r($row[$i]['timestamp'], 1), '</td>' . PHP_EOL;
            echo '</tr>' . PHP_EOL;
        }
        echo '</table>' . PHP_EOL;
    }
    public function showAll()
    {
        $datas = $this->getAll();
        echo '<table class="pair-table">
            <tr bgcolor="white">
            <th>ID</th>
			<th>SYMBOL</th>
			<th>PRICE</th>
			<th>Date/Time</th>
			</tr>' . PHP_EOL;
        for ($i = 0; $i < count($datas); $i++) {
            echo '<tr>' . PHP_EOL;
            echo '<td>', print_r($datas[$i]['id'], 1), '</td>' . PHP_EOL;
            echo '<td>', print_r($datas[$i]['symbol'], 1), '</td>' . PHP_EOL;
            echo '<td>', print_r($datas[$i]['price'], 1), '</td>' . PHP_EOL;
            echo '<td>', print_r($datas[$i]['timestamp'], 1), '</td>' . PHP_EOL;
            echo '</tr>' . PHP_EOL;
        }
        echo '</table>' . PHP_EOL;
    }
}
