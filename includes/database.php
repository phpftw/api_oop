<?php
require_once 'DBInterface.php';
class Database implements DB
{
    protected $pdo = null;
    public function __construct($servername, $username, $password, $dbname)
    {
        $dsn = "mysql:host=" . $servername . ";dbname=" . $dbname . ";charset=utf8mb4";
        $this->pdo = new PDO($dsn, $username, $password);
    }
    public function query($query)
    {
        return $this->pdo->query($query);
    }
    public function prepare($query)
    {
        return $this->pdo->prepare($query);
    }
}
