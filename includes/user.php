<?php
class User
{
    private $db;
    private $first;
    private $last;
    private $email;
    private $uid;
    private $pwd;
    public function __construct(DB $db)
    {
        $this->db = $db;

    }
    public function getById($id)
    {
        $stmt = $this->db->prepare("SELECT id,first,last,email,user,password,date FROM users WHERE id = :id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $row = $stmt->fetch();
        return $row;
    }
    public function edit($id, $first, $last, $email, $user, $pwd)
    {
        $hashedPwd = password_hash($pwd, PASSWORD_BCRYPT);
        $sql = "UPDATE users SET first=?, last=?, email=?, user=?, password=? WHERE id=?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([$first, $last, $email, $user, $hashedPwd, $id]);
        header("Location: profile.php");
    }
    public function login($uidemail, $pwd)
    {
        $error = null;
        //check for empty fields
        if (empty($uidemail)) {
            $error['uidemail'] = 'Please type in a Username or E-mail';
        }
        if (empty($pwd)) {
            $error['pwd'] = 'Please type in a password';
        }
        //check if username / email is valid
        $stmt = $this->db->prepare("SELECT id,first,last,email,user,password,date FROM users WHERE user = :user OR email = :email");
        $stmt->bindParam(':user', $uidemail);
        $stmt->bindParam(':email', $uidemail);
        $stmt->execute();
        if ($stmt->rowCount() < 1) {
            $error['uidemail'] = 'Invalid Username or E-mail.';
        }
        //if username is valid check if it matches the password
        $row = $stmt->fetch();
        $pwdCheck = password_verify($pwd, $row['password']);
        if ($pwdCheck == false) {
            $error['pwd'] = 'Incorrect password';
        }
        //if the password matches login the user
        elseif ($pwdCheck == true) {
            $_SESSION['id'] = $row['id'];
            $_SESSION['first'] = $row['first'];
            $_SESSION['last'] = $row['last'];
            $_SESSION['email'] = $row['email'];
            $_SESSION['user'] = $row['user'];
            $_SESSION['date'] = $row['date'];

        }
        return $error;

    }
    public function editvalidate($id, $first, $last, $email, $uid, $pwd, $confirmpwd)
    {
        $error = null;
        //check for empty fields
        if (empty($first)) {
            $error['first'] = 'Firstname cannot be empty.';
        }
        if (empty($last)) {
            $error['last'] = 'Lastname cannot be empty. ';
        }
        if (empty($email)) {
            $error['email'] = 'Email cannot be empty';
        }
        if (empty($uid)) {
            $error['uid'] = 'Username cannot be empty';
        }
        if (empty($pwd)) {
            $error['pwd'] = 'Please type in a password';
        }
        //check if the password match
        if ($pwd != $confirmpwd) {
            $error['pwd'] = 'Passwords do not match. ';
        }
        //check if input characters are valid
        if (!preg_match("/^[a-zA-Z]*$/", $first)) {
            $error['first'] = 'Invalid characters for Firstname';
        }
        if (!preg_match("/^[a-zA-Z]*$/", $last)) {

            $error['last'] = 'Invalid characters for Lastname';
        }
        //check if email format is valid
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $error['email'] = 'Invalid E-mail format.';
        }
        //check if username exists
        $stmtU = $this->db->prepare("SELECT id,user FROM users WHERE user = :user");
        $stmtU->bindParam(':user', $uid);
        $stmtU->execute();
        $row = $stmtU->fetch();
        if ($stmtU->rowCount() > 0 && $id != $row['id']) {
            $error['uid'] = 'Username already exists.';
        }
        //check if email exists
        $stmtE = $this->db->prepare("SELECT id,email FROM users WHERE email = :email");
        $stmtE->bindParam(':email', $email);
        $stmtE->execute();
        $row2 = $stmtE->fetch();
        if ($stmtE->rowCount() > 0 && $id != $row2['id']) {
            $error['email'] = 'Email already exists.';
        }
        return $error;
    }
    public function validate($first, $last, $email, $uid, $pwd, $confirmpwd)
    {
        $error = null;
        //check for empty fields
        if (empty($first)) {
            $error['first'] = 'Firstname cannot be empty.';
        }
        if (empty($last)) {
            $error['last'] = 'Lastname cannot be empty. ';
        }
        if (empty($email)) {
            $error['email'] = 'Email cannot be empty';
        }
        if (empty($uid)) {
            $error['uid'] = 'Username cannot be empty';
        }
        if (empty($pwd)) {
            $error['pwd'] = 'Please type in a password';
        }
        //check if the password match
        if ($pwd != $confirmpwd) {
            $error['pwd'] = 'Passwords do not match. ';
        }
        //check if input characters are valid
        if (!preg_match("/^[a-zA-Z]*$/", $first)) {
            $error['first'] = 'Invalid characters for Firstname';
        }
        if (!preg_match("/^[a-zA-Z]*$/", $last)) {

            $error['last'] = 'Invalid characters for Lastname';
        }
        //check if email format is valid
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $error['email'] = 'Invalid E-mail format.';
        }
        //check if username exists
        $stmtU = $this->db->prepare("SELECT user FROM users WHERE user = :user");
        $stmtU->bindParam(':user', $uid);
        $stmtU->execute();
        if ($stmtU->rowCount() > 0) {
            $error['uid'] = 'Username already exists.';
        }
        //check if email exists
        $stmtE = $this->db->prepare("SELECT email FROM users WHERE email = :email");
        $stmtE->bindParam(':email', $email);
        $stmtE->execute();
        if ($stmtE->rowCount() > 0) {
            $error['email'] = 'Email already exists.';
        }

        $this->first = trim($first);
        $this->last = trim($last);
        $this->email = trim($email);
        $this->uid = trim($uid);
        $this->pwd = trim($pwd);

        return $error;
    }

    public function add()
    {

        $hashedPwd = password_hash($this->pwd, PASSWORD_BCRYPT);
        $current_time = date('Y-m-d h:m');
        $statement = $this->db->prepare("INSERT INTO users (first,last,email,user,password,date)
                VALUES (?,?,?,?,?,?)");
        $statement->execute([$this->first, $this->last, $this->email, $this->uid, $hashedPwd, $current_time]);

    }
}
