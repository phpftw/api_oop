<?php
require_once 'adduser.php';
require_once 'header.php';
?>
   <section class="main-container">
    <div class="main-wrapper">
    <h2>Sign Up</h2>
    <form class="signup-form" action="signup.php" method="POST">
<?php if (isset($error['first'])): ?><span class="errormsg"><?=$error['first']?></span><?php endif;?>
    <input type="text" name="first" placeholder="Firstname" value="<?=$first?>">
<?php if (isset($error['last'])): ?><span class="errormsg"><?=$error['last']?></span><?php endif;?>
    <input type="text" name="last" placeholder="Lastname"value="<?=$last?>">
<?php if (isset($error['email'])): ?><span class="errormsg"><?=$error['email']?></span><?php endif;?>
    <input type="text" name="email" placeholder="E-mail"value="<?=$email?>">
<?php if (isset($error['uid'])): ?><span class="errormsg"><?=$error['uid']?></span><?php endif;?>
    <input type="text" name="uid" placeholder="Username"value="<?=$uid?>">
<?php if (isset($error['pwd'])): ?><span class="errormsg"><?=$error['pwd']?></span><?php endif;?>
    <input type="password" name="pwd" placeholder="Password"value="<?=$pwd?>">
    <input type="password" name="confirmpwd" placeholder="Confirm Password"value="<?=$confirmpwd?>">
    <button type="submit" name="submit">Sign up</button>
    </div>
<?php
require_once 'footer.php';
?>
