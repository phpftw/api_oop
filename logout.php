<?php
  session_start();
if (!isset($_SESSION['id'])){
    header("Location: index.php");
    die();
}else{
    session_unset();
    session_destroy();
    header("Location: index.php");
    die();
}
